package geekbrains.java_1.lesson_3;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final Scanner sc = new Scanner(System.in);
    private static final Random rnd = new Random();

    public static void main(String[] args) {
        //В конце каждого ввода нажимаем Enter
        System.out.println("Введите одно слово...");
        String word = sc.next();
        System.out.println("Ваше введённое слово: '" + word + "'");
        System.out.println("Введите одно целое число...");
        int a = sc.nextInt();
        System.out.println("Вы ввели число: '" + a + "'");
        System.out.println("Введите два целых числа через пробел...");
        a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println("Вы ввели числа " + a + " и " + b);
        System.out.println("Введите данные, будет считана полностью введённая строка...");
        sc.nextLine(); // Если это не написать, то следующая строка получит из консоли конец предыдущей строки, с двумя int
        String line = sc.nextLine();
        System.out.println("Вы ввели строку: '" + line + "'");
        sc.close();

        printNumbers(1);
        printNumbers(1, 2);
        printNumbers(1, 2, 3, 4, 5, 6);
        System.out.println(getRandomString(10));
    }

    private static void printNumbers(int... ints){
        for (int i = 0; i < ints.length; i++) {
            System.out.print(ints[i] + " ");
        }
        System.out.println();
    }

    private static String getRandomString(int length){
        String s = "";
        for (int i = 0; i < length; i++) {
            s += (char)(rnd.nextInt(26) + 65);
        }
        return s;
    }
}
